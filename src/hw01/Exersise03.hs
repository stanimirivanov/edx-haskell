module Main where

xs = [1 .. 5]

init1 xs = tail (reverse xs)

init2 xs = reverse (head (reverse xs))

init3 xs = reverse (tail xs)

init4 xs = take (length xs) xs

init5 xs = reverse (tail (reverse xs))

init6 xs = take (length xs - 1) (tail xs)

init7 xs = drop (length xs -1) xs

-- EXERCISE 3
-- The library function init, which removes the last element from a non-empty list, can be defined in
-- terms of the library functions introduced in this chapter. Choose one or more possible definitions.
main :: IO ()
main = do
    putStrLn ("0: " ++ (show (init xs)))
    putStrLn ("1: " ++ (show (init1 xs)))
    -- putStrLn ("2: " ++ (show (init2 xs)))
    putStrLn ("3: " ++ (show (init3 xs)))
    putStrLn ("4: " ++ (show (init4 xs)))
    putStrLn ("5: " ++ (show (init5 xs)))
    putStrLn ("6: " ++ (show (init6 xs)))
    putStrLn ("7: " ++ (show (init7 xs)))
