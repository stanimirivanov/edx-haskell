module Main where

xs = [1, 2, 3, 4, 5]

last1 xs = drop (length xs - 1) xs

last2 xs = head (drop (length xs - 1) xs)

last3 xs = tail (reverse xs)

last4 xs = xs !! (length xs - 1)

last5 xs = head (drop (length xs) xs)

last6 xs = head (reverse xs)

last7 xs = reverse xs !! (length xs - 1)

-- EXERCISE 2
-- The library function last, which selects the last element of a non-empty list, can be defined in
-- terms of the library functions introduced in this chapter. Choose one or more possible definitions.
main :: IO ()
main = do
    putStr (show "last: ")
    putStrLn (show (last xs))
    putStr (show "last1: ")
    putStrLn (show (last1 xs))
    putStr (show "last2: ")
    putStrLn (show (last2 xs))
    putStr (show "last3: ")
    putStrLn (show (last3 xs))
    putStr (show "last4: ")
    putStrLn (show (last4 xs))
    -- putStr (show "last5: ")
    -- putStrLn (show (last5 xs)) -- runtime error
    putStr (show "last6: ")
    putStrLn (show (last6 xs))
    putStr (show "last7: ")
    putStrLn (show (last7 xs))
