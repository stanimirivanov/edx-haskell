module Main where

xs = [1 .. 10]

qsort1 [] = []
qsort1 (x : xs) = qsort1 larger ++ [x] ++ qsort1 smaller
    where
        smaller = [a | a <- xs, a <= x]
        larger = [b | b <- xs, b > x]

qsort2 [] = []
qsort2 (x : xs) = reverse (qsort2 larger ++ [x] ++ qsort2 smaller)
    where
        smaller = [a | a <- xs, a <= x]
        larger = [b | b <- xs, b > x]

qsort3 [] = []
qsort3 xs = qsort3 larger ++ qsort3 smaller ++ [x]
    where
        x = minimum xs
        smaller = [a | a <- xs, a <= x]
        larger = [b | b <- xs, b > x]

qsort4 [] = []
qsort4 (x : xs) = reverse (qsort4 smaller) ++ [x] ++ reverse (qsort4 larger)
    where
        smaller = [a | a <- xs, a <= x]
        larger = [b | b <- xs, b > x]

qsort5 [] = []
qsort5 (x : xs) = qsort5 larger ++ [x] ++ qsort5 smaller
    where
        larger = [a | a <- xs, a > x || a == x]
        smaller = [b | b <- xs, b < x]

qsort6 [] = []
qsort6 (x : xs) = qsort6 larger ++ [x] ++ qsort6 smaller
    where
        smaller = [a | a <- xs, a < x]
        larger = [b | b <- xs, b > x]

qsort7 [] = []
qsort7 (x : xs) = reverse (reverse (qsort7 smaller) ++ [x] ++ reverse (qsort7 larger))
    where
        smaller = [a | a <- xs, a <= x]
        larger = [b | b <- xs, b > x]

qsort8 [] = []
qsort8 xs = x: qsort8 larger ++ qsort8 smaller
    where
        x = maximum xs
        smaller = [a | a <- xs, a < x]
        larger = [b | b <- xs, b >= x]


-- EXERCISE 7
-- How should the definition of the function qsort be modified so that it produces a reverse sorted
-- version of a list? Choose one or more possible definitions.
main :: IO ()
main = do
    putStrLn ("1: " ++ (show (qsort1 xs)))
    putStrLn ("2: " ++ (show (qsort2 xs)))
    putStrLn ("3: " ++ (show (qsort3 xs)))
    putStrLn ("4: " ++ (show (qsort4 xs)))
    putStrLn ("5: " ++ (show (qsort5 xs)))
    putStrLn ("6: " ++ (show (qsort6 xs)))
    putStrLn ("7: " ++ (show (qsort7 xs)))
--    putStrLn ("8: " ++ (show (qsort8 xs)))
