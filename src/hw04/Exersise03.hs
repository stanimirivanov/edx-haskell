module Main where

factors n = [x | x <- [1 .. n], n `mod` x == 0]

perfects1 n = [x | x <- [1 .. n], isPerfect x]
    where isPerfect num = sum (factors num) == num

perfects2 n = [x | x <- [1 .. n], isPerfect x]
    where isPerfect num = sum (init (factors num)) == num

perfects3 n = [isPerfect x | x <- [1 .. n]]
    where isPerfect num = sum (factors num) == num

-- perfects4 n = [x | x <- [1 .. n], isPerfect x]
--    where isPerfect num = init (factors num) == num

-- EXERCISE 3
-- A positive integer is perfect if it equals the sum of its factors, excluding the number itself.
-- Choose the correct definition of the function perfects :: Int -> [Int] that returns the list
-- of all perfect numbers up to a given limit.
--
--   > perfects 500
--   [6, 28, 496]
main :: IO ()
main = do
    putStrLn ("1: " ++ (show (perfects1 500)))
    putStrLn ("2: " ++ (show (perfects2 500)))
    putStrLn ("3: " ++ (show (perfects3 500)))
    -- putStrLn ("4: " ++ (show (perfects4 500)))


