module Main where

pyths1 n = [(x, y, z) | x <- [1 .. n], y <- [1 .. x], z <- [1 .. y], x ^ 2 + y ^ 2 == z ^ 2]

pyths2 n = [(x, y, z) | x <- [1 .. n], y <- [x .. n], z <- [y .. n], x ^ 2 + y ^ 2 == z ^ 2]

pyths3 n = [(x, y, z) | x <- [1 .. n], y <- [1 .. n], z <- [1 .. n], x ^ 2 + y ^ 2 == z ^ 2]

pyths4 n = [(x, y, (x ^ 2  + y ^ 2)) | x <- [1 .. n], y <- [1 .. n]]

-- EXERCISE 2
-- A triple (x, y, z) of positive integers is pythagorean if x^2+y^2=z^2. Choose the correct 
-- implementation for the function  :: Int -> [(Int, Int, Int)] that returns the list 
-- of all pythagorean triples whose components are at most a given limit.
-- 
--   >  10
--   [(3, 4, 5), (4, 3, 5), (6, 8, 10), (8, 6, 10)]
main :: IO ()
main = do
    putStrLn ("1: " ++ (show (pyths1 10)))
    putStrLn ("2: " ++ (show (pyths2 10)))
    putStrLn ("3: " ++ (show (pyths3 10)))
    putStrLn ("4: " ++ (show (pyths4 10)))


