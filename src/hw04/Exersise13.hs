module Main where

divides :: Int -> Int -> Bool
d `divides` n = d `mod` n == 0

divisors1 x = [d | d <- [1 .. x], x `divides` d]

divisors2 x = [d | d <- [1 .. x], d `divides` x]

divisors3 x = [d | d <- [2 .. x], x `divides` d]

-- divisors4 x = [d | d <- [1 .. x], x divides d]

-- EXERCISE 13
-- Choose the correct definition for the function divisors :: Int -> [Int] that returns the divisors of a natural number.
--
-- For example:
--   divisors 15 = [1, 3, 5, 15]
--
-- The function divides :: Int -> Int -> Bool decides if one integer is divisible by another.
-- (Note: You need to implement this function yourself.)
--
-- For example:
--   divides 15 2 = False
--   divides 15 3 = True
main :: IO ()
main = do
    putStrLn ("1: " ++ (show (divisors1 15)))
    putStrLn ("2: " ++ (show (divisors2 15)))
    putStrLn ("3: " ++ (show (divisors3 15)))
    -- putStrLn ("4: " ++ (show (divisors4 15)))
