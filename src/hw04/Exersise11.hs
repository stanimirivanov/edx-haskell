module Main where

xs = 1 : [x + 1 | x <- xs]

-- EXERCISE 11
-- Evaluating sum [x | x <- [1..10], even x] gives:
main :: IO ()
main = do
    putStrLn ("1: " ++ (show xs))