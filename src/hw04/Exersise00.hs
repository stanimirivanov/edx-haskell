module Main where

-- sum100a = sum [[x * x] | x <- [1 .. 100]]

sum100b = sum [x ^ 2 | x <- [1 .. 100]]

-- sum100c = sum [[const 2 x] | x <- [1 .. 100]]

sum100d = foldl (+) (1) [x ^ 2 | x <- [1 .. 100]]

-- EXERCISE 0
-- Which of these expressions calculates the sum: 1^2+2^2+...+100^2 of the first one hundred integer squares?
main :: IO ()
main = do
    -- putStrLn ("a: " ++ (show sum100a))
    putStrLn ("b: " ++ (show sum100b))
    -- putStrLn ("c: " ++ (show sum100c))
    putStrLn ("d: " ++ (show sum100d))
