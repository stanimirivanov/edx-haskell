module Main where

xs = [1, 2, 3]
ys = [4, 5, 6]

comprehension0 = [(x, y) | x <- xs, y <- ys]

comprehension1 = [z | z <- [[(x, y) | y <- ys] | x <- xs]]

comprehension2 = concat [[[(x, y)] | x <- xs] | y <- ys]

-- comprehension3 = concat [(x, y) | y <- ys] | x <- xs

comprehension4 = concat [[(x, y) | y <- ys] | x <- xs]


-- EXERCISE 4
-- The following list comprehension:
-- [(x, y) | x <- [1, 2, 3], y <- [4, 5, 6]]
-- can be re-expressed using two or more comprehensions with single generators. 
-- Choose the implementation that is equivalent to the one above.
main :: IO ()
main = do
    putStrLn ("0: " ++ (show comprehension0))
    putStrLn ("1: " ++ (show comprehension1))
    putStrLn ("2: " ++ (show comprehension2))
    -- putStrLn ("3: " ++ (show comprehension3))
    putStrLn ("4: " ++ (show comprehension4))


