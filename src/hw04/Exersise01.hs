module Main where

replicate1 n a = [True | _ <- [1 .. n]]

replicate2 n a = [n | _ <- [1 .. n]]

replicate3 n a = [a | _ <- [1 .. a]]

replicate4 n a = [a | _ <- [1 .. n]]

-- EXERCISE 1
-- The library function replicate :: Int -> a -> [a] produces a list of identical elements.
-- Choose one possible implementation for this function. For example:
--
--   > replicate 3 True
--   [True, True, True]
main :: IO ()
main = do
    putStrLn ("0: " ++ (show (replicate 3 False)))
    putStrLn ("1: " ++ (show (replicate1 3 False)))
    putStrLn ("2: " ++ (show (replicate2 3 False)))
    -- putStrLn ("3: " ++ (show (replicate3 3 False)))
    putStrLn ("4: " ++ (show (replicate4 3 False)))


