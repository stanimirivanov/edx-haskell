module Main where

-- EXERCISE 10
-- Evaluating sum [x | x <- [1..10], even x] gives:
main :: IO ()
main = do
    putStrLn ("1: " ++ (show (sum [x | x <- [1 .. 10], even x])))