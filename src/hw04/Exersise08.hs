module Main where

-- EXERCISE 8
-- Evaluating [(x, y) | x <- [1, 2], y <- [1, 2]] gives:
main :: IO ()
main = do
    putStrLn ("1: " ++ (show [(x, y) | x <- [1, 2], y <- [1, 2]]))