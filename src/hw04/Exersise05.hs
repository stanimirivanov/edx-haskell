module Main where

find :: (Eq a) => a -> [(a, b)] -> [b]
find k t = [v | (k', v) <- t, k == k']

positions :: (Eq a) => a -> [a] -> [Int]
positions x xs = [i | (x', i) <- zip xs [0 .. n], x == x']
    where n = length xs - 1


positions1 x xs = find x (zip xs [0 .. n])
    where n = length xs - 1

positions2 x xs = find x xs

-- positions3 x xs = find x (zipWith (+) xs [0 .. n])
--    where n = length xs - 1

positions4 x xs = find n (zip xs [0 .. n])
    where n = length xs - 1

-- EXERCISE 5
-- Redefine the function positions discussed in the lecture, using the function find:
--
-- find :: (Eq a) => a -> [(a, b)] -> [b]
-- find k t = [v | (k', v) <- t, k == k']
--
-- positions :: (Eq a) => a -> [a] -> [Int]
main :: IO ()
main = do
    putStrLn ("0: " ++ (show (positions 1 [1 .. 10])))
    putStrLn ("1: " ++ (show (positions1 1 [1 .. 10])))
    -- putStrLn ("2: " ++ (show (positions2 1 [1 .. 10])))
    -- putStrLn ("3: " ++ (show (positions3 1 [1 .. 10])))
    putStrLn ("4: " ++ (show (positions4 1 [1 .. 10])))


