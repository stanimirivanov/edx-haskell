module Main where

scalarproducts1 xs ys = sum [x * y | x <- xs, y <- ys]

scalarproducts2 xs ys = sum [x * y | (x, y) <- xs `zip` ys]

scalarproducts3 xs ys = product (zipWith (+) xs ys)

-- scalarproducts4 xs ys = sum (product [(x, y) | x <- xs, y <- ys])

-- EXERCISE 6
-- The scalar product of two lists of integers xs and ys of length n is given 
-- by the sum of the products of corresponding integers:
-- 
--   sum ( (xs !! i) * (ys !! i) ) for i = 0 to n-1
--
-- Choose the correct definition of scalarproduct :: [ Int ] -> [ Int ] -> Int that 
-- returns the scalar product of two lists.
-- 
--   > scalarproduct [1, 2, 3] [4, 5, 6]
--   32
main :: IO ()
main = do
    putStrLn ("1: " ++ (show (scalarproducts1 [1, 2, 3] [4, 5, 6])))
    putStrLn ("2: " ++ (show (scalarproducts2 [1, 2, 3] [4, 5, 6])))
    putStrLn ("3: " ++ (show (scalarproducts3 [1, 2, 3] [4, 5, 6])))
    -- putStrLn ("4: " ++ (show (scalarproducts4 [1, 2, 3] [4, 5, 6])))


