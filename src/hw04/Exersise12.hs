module Main where

riffle1 xs ys = concat [[x, y] | x <- xs, y <- ys]

riffle2 xs ys = concat [[x, y] | (x, y) <- xs `zip` ys]

-- riffle3 xs ys = concat [x, y | (x, y) <- xs `zip` ys]

riffle4 xs ys = [x : [y] | x <- xs, y <- ys]

-- EXERCISE 12
-- Choose the correct definition of the function riffle :: [a] -> [a] -> [a] that 
-- takes two lists of the same length and interleaves their elements in turn about order.
--
-- For example:
--   riffle [1, 2, 3] [4, 5, 6] = [1, 2, 3, 4, 5, 6]
main :: IO ()
main = do
    putStrLn ("1: " ++ (show (riffle1 [1, 2, 3] [4, 5, 6])))
    putStrLn ("2: " ++ (show (riffle2 [1, 2, 3] [4, 5, 6])))
    -- putStrLn ("3: " ++ (show (riffle3 [1, 2, 3] [4, 5, 6])))
    putStrLn ("4: " ++ (show (riffle4 [1, 2, 3] [4, 5, 6])))
