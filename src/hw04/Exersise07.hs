module Main where

import Data.Char

let2int :: Char -> Int
let2int c = ord c - ord 'a'

int2let :: Int -> Char
int2let n = chr (ord 'a'  + n)

shift :: Int -> Char -> Char
shift n c
    | isLower c = int2let ((let2int c + n) `mod` 26)
    | otherwise = c

encode :: Int -> String -> String
encode n xs = [shift n x | x <- xs ]


shift2 :: Int -> Char -> Char
shift2 n c
    | isLower c = int2let ((let2int c + n) `mod` 26)
    | isUpper c  = toUpper $ int2let ((let2int (toLower c) + n) `mod` 26)
    | otherwise = c

encode2 :: Int -> String -> String
encode2 n xs = [shift2 n x | x <- xs ]

-- EXERCISE 7
-- Modify the Caesar cipher program to also handle upper-case letters.
-- Given the following string Think like a Fundamentalist Code like a Hacker,
-- encode it with your modified program (using shift size 13) and choose the correct output.
main :: IO ()
main = do
    putStrLn ("1: " ++ (show (encode 13 "Think like a Fundamentalist Code like a Hacker")))
    putStrLn ("1: " ++ (show (encode2 13 "Think like a Fundamentalist Code like a Hacker")))


