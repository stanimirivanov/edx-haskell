module Main where

-- EXERCISE 9
-- Evaluating [x | x <- [1, 2, 3], y <- [1..x]] gives:
main :: IO ()
main = do
    putStrLn ("1: " ++ (show [x | x <- [1, 2, 3], y <- [1 .. x]]))