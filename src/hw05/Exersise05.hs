module Main where

import Prelude hiding(concat)

concat :: [[a]] -> [a]

{-
concat [] = []
concat (xs : xss) = xs : concat xss
-}

concat [] = []
concat (xs : xss) = xs ++ concat xss

{-
concat [] = [[]]
concat (xs : xss) = xs ++ concat xss
-}

{-
concat [[]] = []
concat (xs : xss) = xs ++ concat xss
-}

-- EXERCISE 5
-- Choose the correct definition for the function that concatenates a list of lists
main :: IO ()
main = do
    putStrLn (show (concat [[1 .. 5], [6 .. 9], [10 .. 15]]))