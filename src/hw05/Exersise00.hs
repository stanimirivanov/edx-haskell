module Main where

import Prelude hiding((^))

{-
-- 1 wrong result for n=0
m ^ 0 = 0
m ^ n = m * m ^ (n - 1)
-}

{-
-- 2 OK
m ^ 0 = 1
m ^ n = m * m ^ (n - 1)
-}

{-
-- 3 stack overflow
m ^ 0 = 1
m ^ n = m * m ^ n - 1
-}

{-
-- 4 wrong result
m ^ 0 = 1
m ^ n = n * n ^ (m - 1)
-}

{-
-- 5 OK
m ^ 0 = 1
m ^ n = m * (^) m (n - 1)
-}

{-
-- 6 stack overflow for n=3
m ^ 0 = 1
m ^ n = m * m * m ^ (n - 2)
-}

-- 7 wrong result
m ^ 0 = 1
m ^ n = (m * m) ^ (n - 1)

{-
-- 8 wrong result for n=0
m ^ 0 = 1
m ^ n = m * m ^ (n - 1)
-}

-- EXERCISE 0
-- Choose all correct definitions of the exponentiation operator ^ for non-negative integers (including 0).
main :: IO ()
main = do
    putStrLn (show (5 ^ 3))
