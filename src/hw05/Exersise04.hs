module Main where

import Prelude hiding(and)

{-
-- 1 OK
and [] = True
and (b : bs) = b && and bs
-}

{-
-- 2 OK
and [] = True
and (b : bs)
    | b = and bs
    | otherwise = False
-}

{-
-- 3 wrong result
and [] = False
and (b : bs) = b && and bs
-}

{-
-- 4 wrong result
and [] = False
and (b : bs) = b || and bs
-}

{-
-- 5 OK
and [] = True
and (b : bs)
    | b == False = False
    | otherwise = and bs
-}

{-
-- 6 wrong result
and [] = True
and (b : bs) = b || and bs
-}

-- 7 OK
and [] = True
and (b : bs) = and bs && b

{-
-- 8 wrong result
and [] = True
and (b : bs)
    | b = b
    | otherwise = and bs
-}

-- EXERCISE 4
-- Choose all correct definitions for the function that decides if all logical values in a list are True:
main :: IO ()
main = do
    putStrLn (show (and [True, True, False]))
    putStrLn (show (and [True, True, True]))
