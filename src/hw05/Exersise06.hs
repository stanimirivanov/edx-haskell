module Main where

import Prelude hiding(replicate)

replicate :: Int -> a -> [a]

{-
replicate 1 x = x
replicate n x = x : replicate (n - 1) x
-}

{-
replicate 0 _ = []
replicate n x = replicate (n - 1) x : x
-}

{-
replicate 1 _ = []
replicate n x = replicate (n - 1) x ++ [x]
-}

replicate 0 _ = []
replicate n x = x : replicate (n - 1) x


-- EXERCISE 6
-- Choose the correct definition for the function that produces a list with n identical elements
main :: IO ()
main = do
    putStrLn (show (replicate 5 'w'))