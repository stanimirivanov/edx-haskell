module Main where

import Prelude hiding ((!!))

{-
(x : _) !! 1 = x
(_ : xs) !! n = xs !! (n - 1)
-}

{-
(x : _) !! 0 = x
(_ : xs) !! n = xs !! (n - 1)
[] !! n = 0
-}

(x : _) !! 0 = x
(_ : xs) !! n = xs !! (n - 1)

{-
(x : _) !! 0 = [x]
(_ : xs) !! n = xs !! (n - 1)
-}


-- EXERCISE 7
-- Choose the correct definition for the function that selects the n th element of a list. We start counting at 0.
main :: IO ()
main = do
    putStrLn (show ([1 .. 5] !! 2))