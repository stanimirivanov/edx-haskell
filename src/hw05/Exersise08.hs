module Main where

import Prelude hiding (elem)

elem :: Eq a => a -> [a] -> Bool

elem _ [] = False
elem x (y : ys)
    | x == y = True
    | otherwise = elem x ys

{-
elem :: Eq a => a -> [a] -> Bool
elem _ [] = False
elem x ( y : ys)
   | x == y  = True
   | otherwise = elem x (y : ys)
-}

{-
elem :: Eq a => a -> [a] -> Bool
elem _ [] = True
elem x ( y : ys)
   | x == y = True
   | otherwise elem x ys
-}

{-
elem :: Eq a => a -> [a] -> Bool
elem _ [] = False
elem x ( y : ys) = x == y
-}

-- EXERCISE 8
-- Choose the correct definition for the function that decides if a value is an element of a list:
main :: IO ()
main = do
    putStrLn (show (elem 5 [1 .. 4]))
    putStrLn (show (elem 5 [1 .. 8]))