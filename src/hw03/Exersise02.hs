module Main where

import Prelude hiding ((||))

-- def 1 is ok
-- False || False = False
-- _ || _  = True

-- def 2 is ok
-- False || b = b
-- True || _  = True

-- def 3 is NOT ok
-- b || c
--    | b  == c = True
--    | otherwise = False

-- def 4 is ok
-- b || c
--    | b  == c = b
--    | otherwise = True

-- def 5 is ok
-- b || False = b
-- _ || True  = True

-- def 6 is ok
b || c
   | b  == c = c
   | otherwise = True

-- def 7 is NOT ok
-- b || True = b
-- _ || True  = True

-- def 8 is ok

-- EXERCISE 2
-- Which of the following definitions is correct for the logical disjunction operator || (i.e. OR)?
-- Choose all correct implementations!
--
-- Note: since the || operator is already defined in the Prelude, you have to hide the default
-- implementation in order to test the following code. You can hide the operator by adding the following
-- line to your script:
main :: IO ()
main = do
    putStrLn (show (False || False))
    putStrLn (show (True || False))
    putStrLn (show (False || True))
    putStrLn (show (True || True))