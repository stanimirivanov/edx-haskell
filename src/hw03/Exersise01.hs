module Main where

xs = [1 .. 10]

safetail1 xs = if null xs then [] else tail xs

safetail2 [] = []
safetail2 (_ : xs) = xs

-- safetail3 xs (_ : xs)
--    | null xs = []
--    | otherwise tail xs

safetail4 xs
    | null xs = []
    | otherwise = tail xs

safetail5 xs = tail xs
safetail5 [] = []

safetail6 [] = []
safetail6 xs = tail xs

safetail7 [x] = [x]
safetail7 (_ : xs) = xs

safetail8
  = \ xs ->
    case xs of
      [] -> []
      (_ : xs) -> xs

-- EXERCISE 1
-- Which of the following implementations are valid for a function safetail :: [a] -> [a] that
-- behaves as the library function tail , except that safetail maps the empty list to itself,
-- whereas tail produces an error in this case. Choose all correct implementations!
main :: IO ()
main = do
    putStrLn ("1: " ++ safetail1 [])
    putStrLn ("1: " ++ (show (safetail1 xs)))
    putStrLn ("2: " ++ safetail2 [])
    putStrLn ("2: " ++ (show (safetail2 xs)))
    -- putStrLn ("3: " ++ safetail3 [])
    -- putStrLn ("3: " ++ (show (safetail3 xs)))
    putStrLn ("4: " ++ safetail4 [])
    putStrLn ("4: " ++ (show (safetail4 xs)))
    -- putStrLn ("5: " ++ safetail5 [])
    -- putStrLn ("5: " ++ (show (safetail5 xs)))
    putStrLn ("6: " ++ safetail6 [])
    putStrLn ("6: " ++ (show (safetail6 xs)))
    -- putStrLn ("7: " ++ safetail7 [])
    -- putStrLn ("7: " ++ (show (safetail7 xs)))
    putStrLn ("8: " ++ safetail8 [])
    putStrLn ("8: " ++ (show (safetail8 xs)))
