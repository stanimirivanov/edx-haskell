module Main where

mult x y z = x * y * z

-- mult1 x y z = \ x -> (\y -> (\z -> x * y * z))

-- mult2 = \ x -> (x * \y -> (y * \z -> z))

mult3 = \ x -> (\y -> (\z -> x * y * z))

-- mult4 = ((((\ x -> \y) -> \z) -> x * y) * z)


-- EXERCISE 4
-- Show how the curried function definition mult x y z = x * y * z can be understood in terms of lambda expressions.
main :: IO ()
main = do
    putStrLn ("0: " ++ (show (mult 2 3 4)))
    -- putStrLn ("1: " ++ (show (mult1 2 3 4)))
    -- putStrLn ("2: " ++ (show (mult2 2 3 4)))
    putStrLn ("3: " ++ (show (mult3 2 3 4)))
    -- putStrLn ("4: " ++ (show (mult4 2 3 4)))