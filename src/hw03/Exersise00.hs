module Main where

xs = [1 .. 10]

-- halve1 xs = (take n xs, drop n xs)
--    where n = length xs / 2

halve2 xs = splitAt (length xs `div` 2) xs

halve3 xs = (take (n `div` 2) xs, drop (n `div` 2) xs)
    where n = length xs

-- halve4 xs = splitAt (length xs `div` 2)

halve5 xs = (take n xs, drop (n + 1) xs)
    where n = length xs `div` 2

halve6 xs = splitAt (div (length xs) 2) xs

-- halve7 xs = splitAt (length xs / 2) xs

halve8 xs = (take n xs, drop n xs)
    where n = length xs `div` 2

-- EXERCISE 0
-- Which of the following implementations correctly define a function halve :: [a] -> ([a], [a]) that
-- splits an even-lengthed list into two halves? Choose all correct implementations!
main :: IO ()
main = do
    -- putStrLn ("1: " ++ (show (halve1 xs)))
    putStrLn ("2: " ++ (show (halve2 xs)))
    putStrLn ("3: " ++ (show (halve3 xs)))
    -- putStrLn ("4: " ++ (show (halve4 xs)))
    putStrLn ("5: " ++ (show (halve5 xs)))
    putStrLn ("6: " ++ (show (halve6 xs)))
    -- putStrLn ("7: " ++ (show (halve7 xs)))
    putStrLn ("8: " ++ (show (halve8 xs)))
