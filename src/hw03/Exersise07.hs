module Main where

xs = [1 .. 4]

remove1 n xs = take n xs ++ drop n xs

remove2 n xs = drop n xs ++ take n xs

remove3 n xs = take (n + 1) xs ++ drop n xs

remove4 n xs = take n xs ++ drop (n + 1) xs

-- EXERCISE 7
-- Choose the correct implementation for the function
-- remove :: Int -> [a] -> [a] which takes a number n and a
-- list and removes the element at position n from the list.
main :: IO ()
main = do
    putStrLn ("1: " ++ (show (remove1 0 xs)))
    putStrLn ("2: " ++ (show (remove2 0 xs)))
    putStrLn ("3: " ++ (show (remove3 0 xs)))
    putStrLn ("4: " ++ (show (remove4 0 xs)))