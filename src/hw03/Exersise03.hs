module Main where

import Prelude hiding ((&&))

-- def 1 is ok
-- True && True = True
-- _ && _  = False

-- def 2 is ok
-- a && b = if a then if b then True else False else False

-- def 3 is NOT ok
-- a && b = if not (a) then not (b) else True

-- def 4 is NOT ok
-- a && b = if a then b

-- def 5 is NOT ok
-- a && b = if a then if b then False else True else False

-- def 6 is ok
-- a && b = if a then b else False

-- def 7 is ok
a && b = if b then a else False

-- EXERCISE 3
-- Which of the following definitions is correct for the logical conjunction operator && (i.e. AND)?
-- Choose all correct implementations!
--
-- Note: since the && operator is already defined in the Prelude, you have to hide the default
-- implementation in order to test the following code. You can hide the operator by adding the following
-- line to your script:
main :: IO ()
main = do
    putStrLn (show (False && False))
    putStrLn (show (True && False))
    putStrLn (show (False && True))
    putStrLn (show (True && True))